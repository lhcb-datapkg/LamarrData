# ReleaseNotes

### 2019-10-17 - Lucio Anderlini
Created the data structure 

### 2019-11-21 - Lucio Anderlini
Loaded the parametrization set shown at CHEP2019

### 2021-05-21 - Adam Davis
Add scikinC models for 2016 MagUp/Down
