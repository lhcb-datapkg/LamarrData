2022-08-24 LamarrData v3r0
===

This version is released on `master` branch.
Built relative to LamarrData [v2r0](../-/tags/v2r0), with the following changes:

### New features ~"new feature"

- ~Tracking | Tag v3r0: updated input variables for acceptance&eff models, !5 (@landerli)   
  Includes:
  - Added PID models based on simulation, !6 (@landerli)
  - Updated isMuon models, !7 (@mabarbet)
  - Fixed isMuon issue with low nTracks, !8 (@mabarbet)
  - added some models with WGAN-ALP, !9 (@mabarbet)


- ~Build | Add requirements file to reset version in nightly, !10 (@gcorti)
