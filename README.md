# Lamarr

c.f. https://its.cern.ch/jira/browse/LBCORE-1812

## Structure 
Data is organized in directories with a naming convention
`<subsystem>/<year>/<parameter_set>`

Subsystems are:
 * CaloObjects
 * ChargedPID
 * RecoStats
 * ReleaseNotes
 * Tracking

Years are:
 * 2012
 * 2016


